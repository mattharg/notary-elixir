# ReqNotary

Request creation of a notary for the supplied file.
Usage:
  reqNotary filename

**TODO: Add more description text**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `req_notary` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:req_notary, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/req_notary](https://hexdocs.pm/req_notary).

