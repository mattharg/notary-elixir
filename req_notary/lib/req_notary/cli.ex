defmodule ReqNotary.CLI do
	# TODO:
	#       o Add facility to cat out the Google Sheet
	#       o Use a default name for Google Sheet so everybody has their own - without having to create one ...
	#       o May as well just have the ONE command / program - no need for two - use a flag if you need to...
	# SIMPLE COMMAND LINE TESTING
	#         mix run -e 'ReqNotary.CLI.run(["-h"])'

	require Logger

	@default_hash "NO_HASH_VALUE"   # This is the Clayton's hash value - when we can't create a valid one

	@moduledoc """
	Command-line parsing - viz
	req_notary filename
	"""


	def run(argv) do
		argv
		|> parse_args
		|> process
		Logger.info "Run completed"
		IO.puts ".............Run Completed.........."
	end

	@doc """
	`argv` can be -h or -help which retruns :help
	if not it should be filename
	which is the name of the file to be notarised on the blockchain with details kept in the Google Sheets DB.

	returns 
	{filename, filehash}
	filehash is the 256 hash of the contents of the file, effectively the 'key' for this file in the Google Sheets DB. 
	It's this hash that actually gets notarised on the blockchain and the google sheets DB references that to the actual file, by name.
	or :help if help was requested
	"""

	def parse_args(argv) do
		Logger.info "parse_args():  #{argv} "
		parse = OptionParser.parse(argv, switches: [ help: :boolen ], 
									  					        aliases:  [ h:    :help ])
		case parse do
			{ [ help: true ], _, _ }    -> :help
			{ _, [ filename ], _ }      -> { filename, "filehash" }
			_                           -> :help
		end
	end

	def process(:help) do
		IO.puts """
		NAME
		   req_notary  -- requests notarisation of the filename on the blockchain, via the xxxx API, caching requests and results in a Google Sheet.

		SYNOPSIS
		   req_notary [ -h | -help | <filename> ] 

		DESCRIPTION
		   The filename is read to generate a 256 hashvalue. 
			 This hashvalue is what is notarised on the BitCoin and Ethereum blockchains.
			 This hashvalue is stored in a Goolge Sheet and a request is made via the xxxxx API for notarisation. 
			 The process takes some time, at least 10 mins but potentially hours before there has been sufficient confirmations on the chains. 
			 Ethereum is faster than BitCoin, but still pretty slow. 
			 At a later time, use this program again to retrieve the notorisation value (which occurs again via the xxxx API).
			 Any retrieved value will be cashed in the Google Sheets so any subsequent requests via this program 
			 will simply return the cached results from the Google Sheet. 
			 The Google Sheet is the register of history notarisations and current requests. 

		"""
		System.halt(0)  # Returning the 'OK' value to the shell. 
	end

	def process( {filename} ) do
		IO.puts """
		req_notary: Supplied: #{filename}
		"""
		ReqNotary.Stampery.fetch(filename)
		|> decode_response()
		System.halt(0)  # Returning the 'OK' value to the shell. 
	end

	def process( {filename, filehash} ) do
		IO.puts """
		req_notary: Supplied: #{filename}, #{filehash}
		"""
		ReqNotary.Stampery.fetch(filename)
		|> decode_response()
		System.halt(0)  # Returning the 'OK' value to the shell. 
	end

	def decode_response( {:ok,     body} ), do: body

	def decode_response( {:error, error} )  do
		IO.puts """
		req_notary: Failed: 3
		req_notary: Failed: Body: #{error}
		"""
		System.halt(3)  # Returning the 'OK' value to the shell. 

	end


end
