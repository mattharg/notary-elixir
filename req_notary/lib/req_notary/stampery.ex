defmodule ReqNotary.Stampery do
##  @client_id    [  {"clientId", "3a02792b546265a"} ]                        # These are PRIVATE to the IoT-SI.  FIXME: TODO: need to replace these with DUMMYS
##  @secret       [  {"secret",   "774e1603-327c-4c8f-9302-32ffe342e544"} ]
##  @stamperyURL  [  {"url",      "https://api-prod.stampery.com/d.stampery.com/stamps"} ] 

	def stampery_api_url() do
		"https://api-prod.stampery.com/d.stampery.com/stamps"
	end

	def fetch(hashvalue) do
		# DOC - post(url, body, headers \\ [], options \\ [])
		# DOC - post(binary, any, headers, Keyword.t) ::
		# DOC - 	{:ok, HTTPoison.Response.t | HTTPoison.AsyncResponse.t} |
		# DOC - 	{:error, HTTPoison.Error.t}

		clientId     = "3a02792b546265a"
		clientSecret = "774e1603-327c-4c8f-9302-32ffe342e544"
		postBody     = [ {"hash", hashvalue} ]
		postHeaders  = [ {"Accept", "application/json"} ] 
		postOptions  = [ {:socks5_user, clientId}, {:socks5_pass, clientSecret} ]

		 HTTPoison.post(stampery_api_url(), postBody, postHeaders, postOptions)
		 |> handle_poison_response
	end

	def handle_response({ :ok, %{status_code:           200, body: body}}) do
		{ :ok,    body}    # filter out of the response what we want to process - the 'body'
	end

	def handle_response({ _,   %{status_code:   status_code, body: body}}) do
		{
			status_code |> check_for_error(),
			body        |> Poison.Parser.parse!()
		}
	end

	def handle_response({:error,   %HTTPoison.Error{id: nil,  reason: :timeout}}) do
		{ :error, "Timed Out with ID as nil" }
	end

	def handle_response({ _,   %{status_code:             _, body: body}}) do
		{ :error, body}    # filter out of the response what we want to process - the 'body'
	end


	defp listToString( [] ) do
		""
	end

	defp listToString( [head | tail] ) do
		tailString = listToString(tail)
		"#{head} #{tailString}"
	end

	defp listToString(head) do
		"#{head}"
	end

	def handle_poison_response({:error,   %HTTPoison.Error{id: id,  reason: reason}}) do
		{ :error, "Reason: #{reason}, Id:#{id}}" }
	end

	def handle_poison_response({:ok,      %HTTPoison.Response{body: body,  headers: [headersList], request_url: request_url, status_code: intCode} }) do
		{ :ok, "Code: #{intCode}, Url:#{request_url}, Body:#{body}, Headers:#{listToString(headersList)} " }
	end

	defp check_for_error(200), do: :ok
	defp check_for_error(_),   do: :error

end
