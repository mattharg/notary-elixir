defmodule ReqNotary do
  @moduledoc """
  Documentation for ReqNotary.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ReqNotary.hello
      :world

  """
  def hello do
    :world
  end
end
