defmodule ReqNotary.MixProject do
  use Mix.Project

  def project do
    [
      app: :req_notary,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]      # NO LONGER REQUIRED TO LIST HTTPOISON as AN APP ---  extra_applications: [:logger, :httpoison]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
				{ :httpoison, "~> 0.13.0" },
				{ :poison,    "~> 3.1"    },
    ]
  end
end
