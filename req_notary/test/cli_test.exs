defmodule CliTest do
	use ExUnit.Case
	doctest ReqNotary

	import ReqNotary.CLI, only: [ parse_args: 1 ] 

	test "-h or -help returns what it is supposed to do" do
		assert parse_args(["-h",     "anythingatallatall"]) == :help
		assert parse_args(["-help",  "anythingatallatall"]) == :help
	end

	test "When a correct, existing filename is given, it returns the filehash value" do
		assert parse_args(["aTestFile.txt"     ]) == { "aTestFile.txt", "filehash" }   # We don't know the filehash yet...
	end

end 


